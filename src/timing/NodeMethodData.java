package timing;

import java.util.Vector;

public class NodeMethodData {
	MethodData data;
	Vector<NodeMethodData> children;
	NodeMethodData parent;

	NodeMethodData() {
		data = new MethodData();
		children = new Vector<NodeMethodData>();
		parent = null;
	}

	public NodeMethodData(MethodData data2) {
		data = data2;
		children = new Vector<NodeMethodData>();
		parent = null;
	}
}
