package timing;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Stack;
import java.util.Vector;

public class TimeKeeper {
	private int measureCounter;
	static boolean TimeKeeperActive;
	private static TimeKeeper instance;
	private Vector<NodeMethodData> timeMeasures;

	private TimeKeeper() {
		measureCounter = 0;
		timeMeasures = new Vector<NodeMethodData>();
	}

	public static void activate() {
		TimeKeeperActive = true;
	}

	public static void desactivate() {
		TimeKeeperActive = false;
	}

	static TimeKeeper GetInstance() {
		if (!TimeKeeperActive) // se comprueba aqui para que cualquier metodo
								// que quiera acceder a la instancia, no pueda
								// usarla si no debiera estar disponible
		{
			return null;
		}
		if (instance == null) {
			instance = new TimeKeeper();
		}
		return instance;
	}

	public static void Start(String tag, String methodName) {
		TimeKeeper tk = TimeKeeper.GetInstance();
		if (tk == null) {
			return;
		}
		MethodData data = new MethodData();
		data.tag = tag;
		data.methodName = methodName;
		data.startTime = System.currentTimeMillis();
		data.isStart = true;
		data.count = tk.measureCounter++;
		tk.timeMeasures.add(new NodeMethodData(data));
	}

	public static void Stop(String tag) {
		TimeKeeper tk = TimeKeeper.GetInstance();
		if (tk == null) {
			return;
		}
		MethodData data = new MethodData();
		data.tag = tag;
		data.stopTime = System.currentTimeMillis();
		data.isStart = false;
		data.count = tk.measureCounter++;
		tk.timeMeasures.add(new NodeMethodData(data));
	}

	public static void WriteData(String sFilename) {
		TimeKeeper tk = TimeKeeper.GetInstance();
		if (tk == null) {
			return;
		}
		NodeMethodData dataTree = new NodeMethodData();
		if (!tk.ValidateMeasures()) {
			System.out.println("TimeDebug: ValidateMeasures() failed.");
			return;
		}
		tk.insertElements(dataTree);
		tk.CalculateTimes(dataTree);
		HashMap<String, ProcessedData> outputInfo = new HashMap<String, ProcessedData>();
		tk.ProcessData(dataTree, outputInfo, "");
		if (!tk.ValidateDataTree(dataTree)) {
			System.out.println("TimeDebug: ValidateDataTree() failed.");
			return;
		}
		PrintStream out = System.out;
		out.println("-WriteData-");
		out.print(" CompleteName");
		out.print("\t TotalTime");
		out.print("\t SelfTime");
		out.print("\t AmountCalls");
		out.println();
		for (String name : outputInfo.keySet()) {
			ProcessedData data = outputInfo.get(name);
			out.print(" " + data.completeName);
			out.print("\t " + data.totalTime);
			out.print("\t " + data.selfTime);
			out.print("\t " + data.amountCalls);
			out.println();
		}
	}

	boolean ValidateDataTree(NodeMethodData _oNode) {
		if (!(_oNode.parent == null)) {
			String a = _oNode.data.tag;
			String b = _oNode.parent.data.tag;
			if (a.compareTo(b) == 0) {
				return false;
			}
		}
		for (int nChild = 0; nChild < _oNode.children.size(); nChild++) {
			if (!ValidateDataTree(_oNode.children.get(nChild))) {
				return false;
			}
		}
		return true;
	}

	boolean ValidateMeasures() {
		Stack<String> m_oStack = new Stack<String>();
		for (int nMeasure = 0; nMeasure < timeMeasures.size(); nMeasure++) {
			if (timeMeasures.get(nMeasure).data.isStart) {
				m_oStack.push(timeMeasures.get(nMeasure).data.tag);
			} else {
				if (m_oStack.isEmpty()) {
					return false;
				}
				if (m_oStack.pop() != timeMeasures.get(nMeasure).data.tag) {
					return false;
				}
			}
		}
		if (m_oStack.empty()) {
			return true;
		} else {
			return false;
		}
	}

	private void ProcessData(NodeMethodData node,
			HashMap<String, ProcessedData> outputInfo, String tab) {
		for (NodeMethodData e : node.children) {
			ProcessedData fData = outputInfo.get(e.data.tag);
			if (fData == null) {
				fData = new ProcessedData();
			}
			fData.tag = e.data.tag;
			++fData.amountCalls;
			fData.totalTime += e.data.totalTime;
			fData.selfTime += e.data.selfTime;
			if (tab.compareTo("") == 0) {
				fData.completeName = e.data.tag;
			} else {
				fData.completeName = tab + "." + e.data.tag;
			}
			outputInfo.put(e.data.tag, fData);
			ProcessData(e, outputInfo, fData.completeName);
		}
	}

	private void CalculateTimes(NodeMethodData node) {
		int accumulator = 0;
		for (NodeMethodData e : node.children) {
			accumulator += e.data.totalTime;
			CalculateTimes(e);
		}
		node.data.selfTime = node.data.totalTime - accumulator;
	}

	private void insertElements(NodeMethodData tree) {
		NodeMethodData first = timeMeasures.get(0);
		first.parent = tree;
		first.parent.children.add(first);
		// para los dem�s
		for (int indexTimeMeasures = 0; indexTimeMeasures < timeMeasures.size() - 1; indexTimeMeasures++) {
			NodeMethodData node1 = timeMeasures.get(indexTimeMeasures);
			if (node1.data.isStart) {
				NodeMethodData node2 = timeMeasures.get(indexTimeMeasures + 1);
				if (node2.data.isStart) {
					// start-start
					node2.parent = node1;
					node2.parent.children.add(node2);
				} else {
					// start-end
					node2.parent = node1.parent;
					node1.data.stopTime = node2.data.stopTime;
					node1.data.totalTime = node1.data.stopTime
							- node1.data.startTime;

				}
			} else {
				NodeMethodData node2 = timeMeasures.get(indexTimeMeasures + 1);
				if (node2.data.isStart) {
					// end-start
					node2.parent = node1.parent;
					node1.parent.children.add(node2);
				} else {
					// end-end
					node2.parent = node1.parent.parent;
					node1.parent.data.stopTime = node2.data.stopTime;
					node1.parent.data.totalTime = node1.parent.data.stopTime
							- node1.parent.data.startTime;
				}
			}
		}
	}

	public static void Start(String tag) {
		Start(tag, "");
	}

}
