package timing;

public class ProcessedData {

	ProcessedData() {
		completeName = "";
		tag = "";
		methodName = "";
		amountCalls = 0;
		totalTime = 0;
		selfTime = 0;
	}

	long amountCalls;
	String completeName, tag, methodName;
	long totalTime, selfTime;
}
