package timing;

public class MethodData {
	public MethodData() {
		methodName = "";
		tag = "";
		isStart = false;
		count = 0;
		startTime = 0;
		stopTime = 0;
		totalTime = 0;
		selfTime = 0;
	}

	String methodName;
	public String tag;
	boolean isStart;
	long count, startTime, stopTime, totalTime, selfTime;
}
