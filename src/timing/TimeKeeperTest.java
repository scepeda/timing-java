package timing;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeKeeperTest {
	@Test
	public void test() {
		String sFilename = "bla.txt";
		assertFalse(TimeKeeper.TimeKeeperActive);
		TimeKeeper.desactivate();
		//assertTrue(TimeKeeper.TimeKeeperActive);
		TimeKeeper.Start("test","");
		foo();
		bar();
		gar(0);
		System.out.println(factorial(50));
		TimeKeeper.Stop("test");
		//TimeKeeper.WriteData(sFilename);
		TimeKeeper.activate();
		//assertFalse(TimeKeeper.TimeKeeperActive);
		TimeKeeper.Start("test","");
		foo();
		bar();
		gar(0);
		System.out.println(factorial(50));
		TimeKeeper.Stop("test");
		
		TimeKeeper.Start("test2");
		TimeKeeper.Start("test_in");
		TimeKeeper.Stop("test_in");
		TimeKeeper.Stop("test2");
		
		TimeKeeper a = TimeKeeper.GetInstance();
		TimeKeeper b = TimeKeeper.GetInstance();
		assertEquals(a,b);
		TimeKeeper.WriteData(sFilename);
	}

	void foo() {
		TimeKeeper.Start("foo","");
		bar();
		bar();
		TimeKeeper.Stop("foo");
	}

	void bar() {
		TimeKeeper.Start("bar","");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
		}
		TimeKeeper.Stop("bar");
	}

	void gar(int i) {
		if (i > 5)
			return;
		gar(++i);
		TimeKeeper.Start("gar","");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
		}
		TimeKeeper.Stop("gar");
	}

	double factorial(double num) {
		TimeKeeper.Start("factorial","");
		if (num <= 1) {
			bye();
			TimeKeeper.Stop("factorial");
			return 1;
		}
		TimeKeeper.Stop("factorial");
		return num * factorial(num - 1);
	}

	private void bye() {
		TimeKeeper.Start("bye","");
		System.out.println("bye");
		TimeKeeper.Stop("bye");
	}
}
